# JsonLd Cached

Create an instance of [JsonLd](https://npmjs.com/package/jsonld) with a context cache
and custom fetch.

```javascript
import JsonLdCached from 'jsonld-cached';
import fetch from 'node-fetch';
import activityStreams from './contexts/activitystreams.js';

const contexts = {
	'https://www.w3.org/ns/activitystreams': activitystreams
}

const jsonld = JsonLdCached({ contexts, fetch });
```

## Dependencies
 - jsonld: ^5.2.0
## Modules

<dl>
<dt><a href="#module_jsonld-cached">jsonld-cached</a></dt>
<dd><p>Jsonld Cached</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#Fetch">Fetch</a> : <code>function</code></dt>
<dd><p>An method adhering to the fetch spec</p>
</dd>
<dt><a href="#ContextCache">ContextCache</a> : <code>Object</code> | <code>Map</code> | <code><a href="#Maplike">Maplike</a></code></dt>
<dd><p>An object or map-like for caching contexts</p>
</dd>
<dt><a href="#Maplike">Maplike</a> : <code>Object</code></dt>
<dd><p>An object with set and get functions</p>
</dd>
</dl>

## External

<dl>
<dt><a href="#external_..JsonLd">~JsonLd</a></dt>
<dd><p>The jsonld package</p>
</dd>
</dl>

<a name="module_jsonld-cached"></a>

## jsonld-cached
Jsonld Cached

<a name="module_jsonld-cached..default"></a>

### jsonld-cached~default ⇒ <code>JsonLd</code>
Create an instance of the JsonLD library, with a context object and custom fetch

**Kind**: inner property of [<code>jsonld-cached</code>](#module_jsonld-cached)  
**Returns**: <code>JsonLd</code> - An instance of JSONLD  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Options object |
| options.contexts | [<code>ContextCache</code>](#ContextCache) | A dict or map-like data structure for caching contexts |
| options.fetch | [<code>Fetch</code>](#Fetch) | A fetch-like method for making http requests |

<a name="Fetch"></a>

## Fetch : <code>function</code>
An method adhering to the fetch spec

**Kind**: global typedef  
<a name="ContextCache"></a>

## ContextCache : <code>Object</code> \| <code>Map</code> \| [<code>Maplike</code>](#Maplike)
An object or map-like for caching contexts

**Kind**: global typedef  
<a name="Maplike"></a>

## Maplike : <code>Object</code>
An object with set and get functions

**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| get.key | <code>string</code> | The url to get for |
| set.key | <code>string</code> | The url to set for |
| set.value | <code>string</code> | The value to set |

**Properties**

| Name | Type | Description |
| --- | --- | --- |
| get | <code>function</code> | Get a value from the cache - can be async |
| set | <code>function</code> | Set a value in the cache - can be async |

<a name="external_..JsonLd"></a>

## ~JsonLd
The jsonld package

**Kind**: global external  
**See**: [jsonld](https://www.npmjs.com/package/jsonld)  
