import { expect, jest, test } from '@jest/globals';
import lib, { makeMap, Loader } from './index.js';

test('Creates jsonld instance', ()=>{
	const res = lib({});
	expect(res).toBeInstanceOf(Function);
	expect(Object.keys(res)).toMatchSnapshot();
});

test('Makemap', ()=>{
	const m = new Map();
	expect(makeMap(m)).toBe(m);
});

test('Loader loads from cache', async ()=>{
	const cx = { key: 'val' };
	const docUrl = 'http://example.com';
	const fetch = jest.fn();
	const contexts = new Map([[docUrl, cx]]);
	const loader = Loader({ contexts, fetch });

	expect(await loader(docUrl)).toEqual({
		document: cx,
		documentUrl: docUrl,
		contextUrl: null
	});
	expect(fetch).not.toHaveBeenCalled();
});

test('Loader loads from http', async ()=>{
	const cx = { key: 'val' };
	const docUrl = 'http://example.com';
	const response = {
		json(){
			return cx;
		},
		ok: true
	};
	const fetch = jest.fn(()=>response);
	const contexts = new Map();
	const loader = Loader({ contexts, fetch });

	expect(await loader(docUrl)).toEqual({
		document: cx,
		documentUrl: docUrl,
		contextUrl: null
	});
	expect(fetch).toHaveBeenCalled();
	expect(contexts.get(docUrl)).toEqual(cx);
});

test('Loader throws on failed request', async ()=>{
	const docUrl = 'http://example.com';
	const response = {
		ok: false,
		status: 500,
		statusText: 'Computer says no'
	};
	const fetch = jest.fn(()=>response);
	const contexts = new Map();
	const loader = Loader({ contexts, fetch });

	await expect(loader(docUrl)).rejects.toMatchSnapshot();
});