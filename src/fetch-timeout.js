export function timeoutSignal(timeout, signal) {
	const controller = new AbortController;
	setTimeout(()=>controller.abort(), timeout);

	if(signal) {
		if(signal.aborted) {
			controller.abort();
		} else {
			signal.addEventListener('abort', ()=>controller.abort());
		}
	}

	return controller.signal;
}

export default function FetchTimeout(fetch, timeout) {
	return function(resource, { signal, ...input } = {}) {
		signal = timeoutSignal(timeout, signal);

		return fetch(resource, { ...input, signal });
	};
}