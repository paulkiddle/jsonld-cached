import FetchTimeout, { timeoutSignal } from './fetch-timeout.js';

test('fetch timeout', async ()=> {
	let s;
	const fetch = async (resource, { signal }) => {
		s = signal;
		await new Promise(resolve => signal.onabort = resolve);
		return 'Resource for ' + resource;
	};
	
	const tf = FetchTimeout(fetch, 500);

	const r = tf('example');
	expect(s?.aborted).toBe(false);
	expect(await r).toEqual('Resource for example');
	expect(s.aborted).toBe(true);
});

test('Timeout signal on already timed out', () => {
	const ac = new AbortController();
	ac.abort();

	expect(ac.signal.aborted).toBe(true);

	const signal = timeoutSignal(500, ac.signal);

	expect(signal.aborted).toBe(true);
});


test('Timeout signal early trigger', () => {
	const ac = new AbortController();

	const signal = timeoutSignal(500, ac.signal);

	expect(signal.aborted).toBe(false);

	ac.abort();

	expect(signal.aborted).toBe(true);
});