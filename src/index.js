/**
 * Jsonld Cached
 * @module jsonld-cached
 */

import Jsonld from 'jsonld';
export { default as FetchTimeout } from './fetch-timeout.js';
export const types = {};

const accept = 'application/ld+json, application/json;q=0.9';
const fetchArgs = { headers: { accept }};

/**
 * An error that occured while fetching a JSONLD resource
 * @property {Fetch.Response} response The fetch response object
 */
export class JsonLdFetchError extends Error {
	constructor(desc, response, ...etc) {
		super(desc, ...etc);
		this.response = response;
	}
}

export function Loader({ contexts, fetch }){
	async function loadJson(url){
		const res = await fetch(url, fetchArgs);
		if(!res.ok) {
			throw new JsonLdFetchError(`Fetch request for ${url} failed (${res.status}: ${res.statusText}).`, res);
		}
		const body = await res.json();
		await contexts.set(url, body);
		return body;
	}

	return async function(url){
		const document = await contexts.get(url) ?? await loadJson(url);

		return {
			contextUrl: null, //https://niem.github.io/json/reference/json-ld/context/
			documentUrl: url,
			document
		};
	};
}

export function makeMap(dict={}) {
	if(!dict.get || !dict.set) {
		dict = new Map(Object.entries(dict));
	}
	return dict;
}

/**
 * Create an instance of the JsonLD library, with a context cache and custom fetch
 * @name	default
 * @param	{Object}	options	Options object
 * @param	{ContextCache}	options.contexts	A dict or map-like data structure for caching contexts
 * @param	{Fetch}	options.fetch	A fetch-like method for making http requests
 * @returns	{JsonLd}	An instance of JSONLD
 * @throws	{JsonLdFetchError}	The http request for a resource did not respond with a 2xx status code
 */
export default function JsonLdCached({ contexts, fetch }){
	contexts = makeMap(contexts);

	const loader = Loader({ contexts, fetch });
	const jsonld = Jsonld();
	jsonld.documentLoader = loader;
	return jsonld;
}

/**
 * The jsonld package
 * @external JsonLd
 * @see {@link https://www.npmjs.com/package/jsonld|jsonld}
 */

/**
 * An method adhering to the fetch spec
 * @typedef	{Function} Fetch
 */

/**
 * An object or map-like for caching contexts
 * @typedef	{(Object|Map|Maplike)}	ContextCache
 */

/**
 * An object with set and get functions
 * @typedef	{Object}	Maplike
 * @property	{Function}	get	Get a value from the cache - can be async
 * @param	{string}	get.key	The url to get for
 * @property	{Function}	set	Set a value in the cache - can be async
 * @param	{string}	set.key	The url to set for
 * @param	{string}	set.value	The value to set
 */
